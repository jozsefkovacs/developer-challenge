<?php

$towns = explode("\n", file_get_contents('towns'));

$numberOfTowns = 5;
$travellingTowns = [];

for($i = 0; $i < $numberOfTowns; $i++) {
    $travellingTowns []= $towns[array_rand($towns)];
}

$distances = [];

foreach($travellingTowns as $travellingTownFrom) {
    $distances[$travellingTownFrom] = [];
    foreach($travellingTowns as $travellingTownTo) {


        if ($travellingTownFrom != $travellingTownTo) {
            if (isset($distances[$travellingTownTo][$travellingTownFrom])) {
                $distances[$travellingTownFrom][$travellingTownTo] = $distances[$travellingTownTo][$travellingTownFrom];
            } else {
                $distances[$travellingTownFrom][$travellingTownTo] = rand(0, 100);
            }
        }
    }
}

$data = [
    'towns' => $travellingTowns,
    'distances' => $distances
];

file_put_contents('paths.json', json_encode($data, JSON_PRETTY_PRINT));
